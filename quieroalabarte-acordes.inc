\context ChordNames
	\chords {
		\set majorSevenSymbol = \markup { "maj7" }
		\set chordChanges = ##t

		R2

		% quiero alabaaarte...
		e2 e2 e2 e2
		gis4.:m cis8:m ~ cis2:m gis4.:m cis8:m ~ cis2:m
		a2 a2 fis2:m fis2:m
		b2 b2 b2 b2

		% quiero abrazaaarte...
		e2 e2 e2 e2
		gis4.:m cis8:m ~ cis2:m gis4.:m cis8:m ~ cis2:m
		a2 a2 fis2:m fis2:m
		b2 b2 b2 b2

		% y contemplaaarte...
		e2 e2 e2 e2
		gis4.:m cis8:m ~ cis2:m gis4.:m cis8:m ~ cis2:m
		a2 a2 fis2:m fis2:m
		b2 b2 b2 b2

		% finale
		e2 e2 e2
	}
