\context Staff = "tenor" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Tenor"
	\set Staff.shortInstrumentName = "T."
	\set Staff.midiInstrument = "Voice Oohs"
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-tenor" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\clef "treble_8"
		\key e \major

		b, 8. dis fis 8  |
		fis 4. ( e 8 )  |
		e 4. r8  |
		a 8 a 16 a 8 gis 16 gis 8  |
%% 5
		r8 gis fis e  |
		dis 4. ( cis 8 )  |
		cis 4. r8  |
		fis 8 fis 16 fis 8 e 16 e 8 ~  |
		e 8 r r cis  |
%% 10
		gis 8. fis 16 fis e 8 fis 16 ~  |
		fis 4 r8 cis  |
		gis 8. fis 16 fis e 8 fis 16 ~  |
		fis 16 r gis 8 fis e  |
		gis 4. ( fis 8 )  |
%% 15
		fis 2  |
		R2  |
		r4 r8 b  |
		b 4 b 8 a 16 gis ~  |
		gis 16 gis 8. r4  |
%% 20
		b 8 b b a 16 gis ~  |
		gis 4 r8 b  |
		b 4 b 8 a 16 gis ~  |
		gis 16 gis 8. r8 r16 b  |
		b 8 b b a 16 gis ~  |
%% 25
		gis 4 r  |
		cis' 4 b 16 a ~ a a ~  |
		a 4 r  |
		cis' 8. b 16 b a 8 a 16 ~  |
		a 16 r fis 8 gis a  |
%% 30
		b 4.. a 16  |
		a 2  |
		R2  |
		b, 8. dis fis 8  |
		fis 4. ( e 8 )  |
%% 35
		e 4. r8  |
		a 8 a 16 a 8 gis 16 gis 8  |
		r8 gis fis e  |
		dis 4. ( cis 8 )  |
		cis 4. r8  |
%% 40
		fis 8 fis 16 fis 8 e 16 e 8 ~  |
		e 8 r r cis  |
		gis 8. fis 16 fis e 8 fis 16 ~  |
		fis 4 r8 cis  |
		gis 8. fis 16 fis e 8 fis 16 ~  |
%% 45
		fis 16 r gis 8 fis e  |
		gis 4. ( fis 8 )  |
		fis 2  |
		R2  |
		b, 8. dis fis 8  |
%% 50
		fis 4. ( e 8 )  |
		e 2 ~  |
		e 4 r  |
		R2  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-tenor" {
		% quiero alabarte...
		Quie -- "ro a" -- la -- bar __ te
		más y más a -- ún,
		quie -- "ro a" -- la -- bar __ te
		más y más a -- ún, __
		bus -- car tu vo -- lun -- tad, __
		tu gra -- cia co -- no -- cer, __
		quie -- "ro a" -- la -- bar __ te. __

		% quiero abrazarte...
		Las a -- ves del cie __ lo
		can -- tan pa -- ra ti, __
		las bes -- tias del cam __  po
		re -- fle -- jan tu po -- der. __
		Quie -- ro can -- tar, __
		quie -- ro le -- van -- tar __
		mis ma -- nos ha -- cia ti.

		% y contemplarte...
		Y con -- tem -- plar __ te
		más y más a -- ún,
		y con -- tem -- plar __ te
		más y más a -- ún, __
		bus -- car tu vo -- lun -- tad, __
		tu gra -- cia co -- no -- cer, __
		y con -- tem -- plar __ te. __

		Quie -- "ro a" -- la -- bar __ te. __
	}
>>
