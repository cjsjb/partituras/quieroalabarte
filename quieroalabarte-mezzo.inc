\context Staff = "mezzo" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Mezzosoprano"
	\set Staff.shortInstrumentName = "M."
	\set Staff.midiInstrument = "Voice Oohs"
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-mezzo" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\clef "treble"
		\key e \major

		b 8. dis' fis' 8  |
		fis' 4. ( e' 8 )  |
		e' 4. r8  |
		fis' 8 fis' 16 fis' 8 e' 16 e' 8  |
%% 5
		r8 gis' fis' e'  |
		dis' 4. ( cis' 8 )  |
		cis' 4. r8  |
		dis' 8 dis' 16 dis' 8 cis' 16 cis' 8 ~  |
		cis' 8 r r cis'  |
%% 10
		gis' 8. fis' 16 fis' e' 8 fis' 16 ~  |
		fis' 4 r8 cis'  |
		gis' 8. fis' 16 fis' e' 8 fis' 16 ~  |
		fis' 16 r e' 8 dis' cis'  |
		e' 4 ( dis' )  |
%% 15
		cis' 4 ( e'  |
		dis' 4. ) r8  |
		b 8. dis' fis' 8  |
		fis' 4. ( e' 8 )  |
		e' 4. r8  |
%% 20
		fis' 8 fis' 16 fis' 8 e' 16 e' 8  |
		r8 gis' fis' e'  |
		dis' 4. ( cis' 8 )  |
		cis' 4. r8  |
		dis' 8 dis' 16 dis' 8 cis' 16 cis' 8 ~  |
%% 25
		cis' 8 r r cis'  |
		gis' 8. fis' 16 fis' e' 8 fis' 16 ~  |
		fis' 4 r8 cis'  |
		gis' 8. fis' 16 fis' e' 8 fis' 16 ~  |
		fis' 16 r e' 8 dis' cis'  |
%% 30
		e' 4 ( dis' )  |
		cis' 4 ( e'  |
		dis' 4. ) r8  |
		b 8. dis' fis' 8  |
		fis' 4. ( e' 8 )  |
%% 35
		e' 4. r8  |
		fis' 8 fis' 16 fis' 8 e' 16 e' 8  |
		r8 gis' fis' e'  |
		dis' 4. ( cis' 8 )  |
		cis' 4. r8  |
%% 40
		dis' 8 dis' 16 dis' 8 cis' 16 cis' 8 ~  |
		cis' 8 r r cis'  |
		gis' 8. fis' 16 fis' e' 8 fis' 16 ~  |
		fis' 4 r8 cis'  |
		gis' 8. fis' 16 fis' e' 8 fis' 16 ~  |
%% 45
		fis' 16 r e' 8 dis' cis'  |
		e' 4 ( dis' )  |
		cis' 4 ( e'  |
		dis' 4. ) r8  |
		b 8. dis' fis' 8  |
%% 50
		fis' 4. ( e' 8 )  |
		e' 2 ~  |
		e' 4 r  |
		R2  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-mezzo" {
		% quiero alabarte...
		Quie -- "ro a" -- la -- bar __ te
		más y más a -- ún,
		quie -- "ro a" -- la -- bar __ te
		más y más a -- ún, __
		bus -- car tu vo -- lun -- tad, __
		tu gra -- cia co -- no -- cer, __
		quie -- "ro a" -- la -- bar __ te. __

		% quiero abrazarte...
		Quie -- "ro a" -- bra -- zar __ te
		más y más a -- ún,
		quie -- "ro a" -- bra -- zar __ te
		más y más a -- ún, __
		bus -- car tu vo -- lun -- tad, __
		tu gra -- cia co -- no -- cer, __
		quie -- "ro a" -- bra -- zar __ te. __

		% y contemplarte...
		Y con -- tem -- plar __ te
		más y más a -- ún,
		y con -- tem -- plar __ te
		más y más a -- ún, __
		bus -- car tu vo -- lun -- tad, __
		tu gra -- cia co -- no -- cer, __
		y con -- tem -- plar __ te. __

		Quie -- "ro a" -- la -- bar __ te. __
	}
>>
